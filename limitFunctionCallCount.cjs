function limitFunctionCallCount(cb, n=1){
    let counter = 0;
    if(arguments.length < 2){
        throw new Error("Not enough parameters");
    }
    function limit(){       
        const arg = Object.values(arguments); 
        if(counter < n){
            counter += 1; 
            const res = cb(...arg);            
            // console.log(counter, n);
            return res;
        }
        else{
            return null;
        }
    }
    return limit;
}
module.exports = limitFunctionCallCount;