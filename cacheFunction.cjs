function cacheFunction(cb){
    let cache = {}; 
    if(arguments.length < 1){
        throw new Error("Not enough parameters");
    }
    function cachef(){     
        // console.log(arguments);
        // console.log(cache[arguments]) ;
        const arg = Object.values(arguments);
        // console.log(arg);
        if(cache[arg] != undefined){
            console.log("result is already present in cache");
            return cache[arg];
        }
        console.log("new result is to be obtained");
        // console.log(typeof(arguments));
        if(arg.length == 0){
            cache[arg] = cb();
        }
        else
            cache[arg] = cb(...arg);
        return cache[arg];
    }
    return cachef;
}
module.exports = cacheFunction;