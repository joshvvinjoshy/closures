function counterFactory(){
    let counter = 0;
    const obj = {
        increment(){
            return counter += 1;
        },
        decrement(){
            return counter -= 1;
        }
    };
    return obj;
}
module.exports = counterFactory;