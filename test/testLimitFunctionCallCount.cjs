const limitFunctionCallCount = require('../limitFunctionCallCount.cjs');
function cb(){
    // console.log(arguments);
    return "hello world";
}
try{
    const limit = limitFunctionCallCount(cb,4);
    console.log(limit());
    console.log(limit());
    console.log(limit());
    console.log(limit());
    console.log(limit());
}
catch(error){
    console.log(error);
}


