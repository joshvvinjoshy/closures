const cacheFunction = require('../cacheFunction.cjs');
function cb(name=''){
    // console.log(arguments);
    return "hello " + name;
}
try{
    const ch = cacheFunction(cb);
    console.log(ch());
    console.log(ch('prateek'));
    console.log(ch('Joshvvin'));
    console.log(ch('prateek'));
}
catch(error){
    console.log(error);
}